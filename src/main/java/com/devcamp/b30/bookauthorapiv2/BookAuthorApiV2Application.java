package com.devcamp.b30.bookauthorapiv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookAuthorApiV2Application {

	public static void main(String[] args) {
		SpringApplication.run(BookAuthorApiV2Application.class, args);
	}

}
