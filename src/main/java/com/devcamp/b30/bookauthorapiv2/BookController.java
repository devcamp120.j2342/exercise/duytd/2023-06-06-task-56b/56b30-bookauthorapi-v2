package com.devcamp.b30.bookauthorapiv2;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.b30.bookauthorapiv2.models.Author;
import com.devcamp.b30.bookauthorapiv2.models.Book;

@RestController
@RequestMapping("/api")
public class BookController {
    @CrossOrigin
    @GetMapping("/books")
    public ArrayList<Book> getBooks(){
        Author author1 = new Author("Hồ Chí Minh", "abc@gmail.com", 'M');
        Author author2 = new Author("Nguyễn Phong", "Phong@gmail.com", 'M');
        Author author3 = new Author("Trần Thanh Hải", "Hai@gmail.com", 'M');
        Author author4 = new Author("Thích Nhất Hạnh", "nhathanhc@gmail.com", 'M');
        Author author5 = new Author("Nguyễn Việt Long", "vietlong@gmail.com", 'M');
        Author author6 = new Author("Nguyễn Du", "abc@gmail.com", 'M');

        System.out.println(author1);
        System.out.println(author2);
        System.out.println(author3);
        System.out.println(author4);
        System.out.println(author5);
        System.out.println(author6);

        ArrayList<Author> arrListAuthor1 = new ArrayList<Author>();
        ArrayList<Author> arrListAuthor2 = new ArrayList<Author>();
        ArrayList<Author> arrListAuthor3 = new ArrayList<Author>();

        arrListAuthor1.add(author1);
        arrListAuthor1.add(author2);

        arrListAuthor2.add(author3);
        arrListAuthor2.add(author4);

        arrListAuthor3.add(author5);
        arrListAuthor3.add(author6);

        Author[] authors1 = arrListAuthor1.toArray(new Author[0]);
        Author[] authors2 = arrListAuthor2.toArray(new Author[1]);
        Author[] authors3 = arrListAuthor3.toArray(new Author[2]);
    
        Book book1 = new Book("Nhật ký trong tu", authors1, 104500, 1);
        Book book2 = new Book("Tiếng chim hót trong bụi mận đỏ", authors2, 75000, 3);
        Book book3 = new Book("Đắc nhân tâm", authors3, 125000, 2);

        System.out.println(book1);
        System.out.println(book2);
        System.out.println(book3);

        ArrayList<Book> arrListbooks = new ArrayList<Book>();
        arrListbooks.add(book1);
        arrListbooks.add(book2);
        arrListbooks.add(book3);

        return arrListbooks;
    }
}
